<?php

/**
 *
 * @link              https://parenthesis.io/
 * @since             1.0.0
 * @package           Asap_Translate_Functionalities
 *
 * @wordpress-plugin
 * Plugin Name:       ASAP Translate Func
 * Plugin URI:        https://bitbucket.org/parenthesis/asap-translate-func
 * Description:       DO NOT DEACTIVATE THIS PLUGIN. It contains code needed by custom functionalities for this website. For support on this plugin please contact <a href="mailto:support@parenthesis.io">support@parenthesis.io</a>.
 * Version:           1.0.0
 * Author:            Parenthesis Tech
 * Author URI:        https://parenthesis.io/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       asap-translate-functionalities
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-asap-translate-functionalities-activator.php
 */
function activate_asap_translate_functionalities() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-asap-translate-functionalities-activator.php';
	Asap_Translate_Functionalities_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-asap-translate-functionalities-deactivator.php
 */
function deactivate_asap_translate_functionalities() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-asap-translate-functionalities-deactivator.php';
	Asap_Translate_Functionalities_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_asap_translate_functionalities' );
register_deactivation_hook( __FILE__, 'deactivate_asap_translate_functionalities' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-asap-translate-functionalities.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_asap_translate_functionalities() {

	$plugin = new Asap_Translate_Functionalities();
	$plugin->run();

}
run_asap_translate_functionalities();
