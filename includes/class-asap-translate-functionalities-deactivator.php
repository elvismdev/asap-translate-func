<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/includes
 * @author     Parenthesis Tech <team@parenthesis.io>
 */
class Asap_Translate_Functionalities_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
