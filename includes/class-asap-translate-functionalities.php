<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/includes
 * @author     Parenthesis Tech <team@parenthesis.io>
 */
class Asap_Translate_Functionalities {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Asap_Translate_Functionalities_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'asap-translate-functionalities';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Asap_Translate_Functionalities_Loader. Orchestrates the hooks of the plugin.
	 * - Asap_Translate_Functionalities_i18n. Defines internationalization functionality.
	 * - Asap_Translate_Functionalities_Admin. Defines all hooks for the admin area.
	 * - Asap_Translate_Functionalities_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-asap-translate-functionalities-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-asap-translate-functionalities-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-asap-translate-functionalities-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-asap-translate-functionalities-public.php';

		$this->loader = new Asap_Translate_Functionalities_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Asap_Translate_Functionalities_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Asap_Translate_Functionalities_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Asap_Translate_Functionalities_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_filter( 'login_redirect', $plugin_admin, 'p_asapt_login_redirect', 10, 3 );
		// $this->loader->add_filter( 'gform_upload_path', $plugin_admin, 'p_asapt_change_upload_path', 10, 2 );
		$this->loader->add_filter( 'gravityflow_status_args', $plugin_admin, 'p_asapt_display_translators_column', 10, 1 );
		$this->loader->add_action( 'gform_after_update_entry', $plugin_admin, 'p_asapt_send_to_dropbox_on_update', 10, 2 );
		// $this->loader->add_action( 'publish_invoice', $plugin_admin, 'p_asapt_after_invoice_publish', 10, 2 );
		$this->loader->add_filter( 'wp_insert_post_data', $plugin_admin, 'p_asapt_invoice_id_as_title', 99, 2 );
		$this->loader->add_action( 'edit_form_after_title', $plugin_admin, 'p_asapt_edit_screen_invoice_payment_url' );
		$this->loader->add_filter( 'manage_invoice_posts_columns', $plugin_admin, 'p_asapt_columns_head_only_invoices', 10 );
		$this->loader->add_action( 'manage_invoice_posts_custom_column', $plugin_admin, 'p_asapt_columns_content_only_invoices', 10, 2 );
		$this->loader->add_action( 'gravityflow_workflow_complete', $plugin_admin, 'p_asapt_create_invoice_from_quote', 10, 3 );
		$this->loader->add_filter( 'wp_mail_from', $plugin_admin, 'p_asapt_wp_mail_from' );
		$this->loader->add_filter( 'wp_mail_from_name', $plugin_admin, 'p_asapt_wp_mail_from_name' );
		$this->loader->add_filter( 'wp_mail_content_type', $plugin_admin, 'p_asapt_set_html_mail_content_type' );

		// Filter pages quantity field value to not show pricing at all.
		$this->loader->add_filter( 'gform_entry_field_value', $plugin_admin, 'p_asapt_trim_price', 10, 4 );

		// Enable field label visibility settings.
		$this->loader->add_filter( 'gform_enable_field_label_visibility_settings', $plugin_admin, '__p_asapt_return_true' );


	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Asap_Translate_Functionalities_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_filter( 'query_vars', $plugin_public, 'p_asapt_add_invoice_query_var' );
		// $this->loader->add_filter( 'gform_field_value_invoice_amount', $plugin_public, 'p_asapt_gf_filter_invoice_amount' );
		$this->loader->add_filter( 'gform_field_value_order_translation_email', $plugin_public, 'p_asapt_gf_filter_order_translation_email' );
		$this->loader->add_filter( 'gform_field_value_order_translation_from_language', $plugin_public, 'p_asapt_gf_filter_order_translation_from_language' );
		$this->loader->add_filter( 'gform_field_value_order_translation_to_language', $plugin_public, 'p_asapt_gf_filter_order_translation_to_language' );
		$this->loader->add_filter( 'gform_field_value_order_translation_first_name', $plugin_public, 'p_asapt_gf_filter_order_translation_first_name' );
		$this->loader->add_filter( 'gform_field_value_order_translation_last_name', $plugin_public, 'p_asapt_gf_filter_order_translation_last_name' );
		$this->loader->add_filter( 'gform_field_value_order_translation_invoice_amount', $plugin_public, 'p_asapt_gf_filter_order_translation_invoice_amount' );
		$this->loader->add_filter( 'gform_pre_render_1', $plugin_public, 'p_asapt_set_conditional_logic_pages_to_translate' );
		$this->loader->add_filter( 'gform_pre_validation_1', $plugin_public, 'p_asapt_set_conditional_logic_pages_to_translate' );
		$this->loader->add_filter( 'gform_pre_submission_filter_1', $plugin_public, 'p_asapt_set_conditional_logic_pages_to_translate' );
		$this->loader->add_filter( 'gform_pre_render_1', $plugin_public, 'p_asapt_set_conditional_logic_invoice_total' );
		$this->loader->add_filter( 'gform_pre_validation_1', $plugin_public, 'p_asapt_set_conditional_logic_invoice_total' );
		$this->loader->add_filter( 'gform_pre_submission_filter_1', $plugin_public, 'p_asapt_set_conditional_logic_invoice_total' );
		$this->loader->add_filter( 'gform_field_value_order_translation_phone', $plugin_public, 'p_asapt_gf_filter_order_translation_phone' );
		$this->loader->add_filter( 'gform_field_value_order_translation_street_address', $plugin_public, 'p_asapt_gf_filter_order_translation_street_address' );
		$this->loader->add_filter( 'gform_field_value_order_translation_city', $plugin_public, 'p_asapt_gf_filter_order_translation_city' );
		$this->loader->add_filter( 'gform_field_value_order_translation_state', $plugin_public, 'p_asapt_gf_filter_order_translation_state' );
		$this->loader->add_filter( 'gform_field_value_order_translation_zip', $plugin_public, 'p_asapt_gf_filter_order_translation_zip' );
		$this->loader->add_filter( 'gform_field_value_order_translation_country', $plugin_public, 'p_asapt_gf_filter_order_translation_country' );
		$this->loader->add_filter( 'gform_field_value_order_translation_delivery', $plugin_public, 'p_asapt_gf_filter_order_translation_delivery' );
		$this->loader->add_filter( 'gform_field_value_order_translation_additional_info', $plugin_public, 'p_asapt_gf_filter_order_translation_additional_info' );
		$this->loader->add_filter( 'gform_field_value_order_translation_invoice_pages_to_translate', $plugin_public, 'p_asapt_gf_filter_order_translation_invoice_pages_to_translate' );
		$this->loader->add_filter( 'gform_pre_render_1', $plugin_public, 'p_asapt_set_conditional_pages_to_translate' );
		$this->loader->add_filter( 'gform_pre_validation_1', $plugin_public, 'p_asapt_set_conditional_pages_to_translate' );
		$this->loader->add_filter( 'gform_pre_submission_filter_1', $plugin_public, 'p_asapt_set_conditional_pages_to_translate' );
		$this->loader->add_filter( 'gform_pre_render_1', $plugin_public, 'p_asapt_add_readonly_script' );
		$this->loader->add_filter( 'gform_field_css_class_6', $plugin_public, 'p_asapt_add_gf_invisible', 10, 3 );
		$this->loader->add_filter( 'gform_pre_render_1', $plugin_public, 'p_asapt_set_conditional_delivery_method' );
		$this->loader->add_filter( 'gform_pre_validation_1', $plugin_public, 'p_asapt_set_conditional_delivery_method' );
		$this->loader->add_filter( 'gform_pre_submission_filter_1', $plugin_public, 'p_asapt_set_conditional_delivery_method' );
		$this->loader->add_filter( 'gform_field_value_order_translation_invoice_delivery_method', $plugin_public, 'p_asapt_gf_filter_order_translation_invoice_delivery_method' );

		// Clear dollar symbol from returned order total value.
		$this->loader->add_filter( 'gform_merge_tag_filter', $plugin_public, 'p_asapt_clear_dollar_sign', 10, 5 );

		// Custom page title for /certified-translation page.
		$this->loader->add_filter( 'pre_get_document_title', $plugin_public, 'p_asapt_filter_page_title_tag' );

		// Prevent to close the order translation tab with JS.
		$this->loader->add_action( 'wp_footer', $plugin_public, 'p_asapt_alert_on_tab_close', 100 );



	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Asap_Translate_Functionalities_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
