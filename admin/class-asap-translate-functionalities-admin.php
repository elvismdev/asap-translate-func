<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/admin
 * @author     Parenthesis Tech <team@parenthesis.io>
 */
class Asap_Translate_Functionalities_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of thmuis class should be passed to the run() function
		 * defined in Asap_Translate_Functionalities_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Asap_Translate_Functionalities_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/asap-translate-functionalities-admin.css', array(), $this->version, 'all' );

		if ( current_user_can( 'translator' ) ) {
			wp_enqueue_style( $this->plugin_name . '-translator', plugin_dir_url( __FILE__ ) . 'css/asap-translate-functionalities-admin-translator.css', array(), $this->version, 'all' );
		}

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Asap_Translate_Functionalities_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Asap_Translate_Functionalities_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/asap-translate-functionalities-admin.js', array( 'jquery' ), $this->version, false );

		$screen = get_current_screen();
		if( $screen->post_type=='invoice' ) {
			wp_enqueue_script( $this->plugin_name . 'clipboardjs', get_stylesheet_directory_uri() . '/bower_components/clipboard/dist/clipboard.min.js', array(  ), '1.5.10', false );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/asap-translate-functionalities-admin.js', array( 'jquery' ), $this->version, false );
		}

	}

	/**
	  * Redirect translators to translations inbox page at login.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_login_redirect( $redirect_to, $request, $user ) {
		if ( is_array( $user->roles ) ) {
			if ( in_array( 'translator', $user->roles ) ) {
				return admin_url( 'admin.php?page=gravityflow-inbox' );
			} else {
				return admin_url();
			}
		}
	}

	/**
	  * Change default gravity forms upload directory.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_change_upload_path( $path_info, $form_id ) {
		$path_info['path'] = '/home/ofnsecure/public_html/asaptranslate.com/raw-files/';
		$path_info['url'] = get_site_url() . '/raw-files/';
		return $path_info;
	}

	/**
	  * Display Translators column in status page for Gravity Flow.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_display_translators_column( $args ) {
		$args['field_ids'][] = 17;
		$args['constraint_filters']['form_id'] = 1;

		return $args;
	}

	/**
	  * Process Dropbox feed for admin on update entry.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_send_to_dropbox_on_update( $form, $entry_id ) {
		if ( function_exists( 'gf_dropbox' ) ) {
			$entry = GFAPI::get_entry( $entry_id );
			gf_dropbox()->maybe_process_feed( $entry, $form );
		}
	}

	/**
	  * Set invoice ID as the invoice/post title.
	  *
	  * @since    1.0.0
	  */
	function p_asapt_invoice_id_as_title( $data , $postarr ) {
		if ( $data['post_type'] == 'invoice' ) {
			$data['post_title'] = $postarr['ID'];
		}
		return $data;
	}

	/**
	  * Send an invoice link by email to customer.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_after_invoice_publish( $ID, $post ) {

		// Process email to customer.

		// Get client's first name
		$name =  get_post_meta( $ID, 'wpcf-invoice_client_first_name', true );
		// Get client's email
		$to = get_post_meta( $ID, 'wpcf-invoice_client_email', true );
		// Get Order Translation page link with the Invoice ID passed as query string
		$order_translation_link = esc_url( add_query_arg( 'inv', $ID, site_url( '/order-translation/' ) ) );
		// Set email subject
		$subject = 'ASAP Translate - Invoice #' . $ID;
		// Set email message
		$message = 'Hi, ' . $name . '. An invoice was just created for you on ASAP Translate. Click here to pay your invoice: ' . $order_translation_link;
		// Send email
		wp_mail( $to, $subject, $message );
	}

	/**
	  * Send an invoice link by email to customer.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_edit_screen_invoice_payment_url( $post ) {
		$screen = get_current_screen();
		if( $screen->post_type=='invoice' && $screen->id=='invoice' ) {
			$this->p_asapt_invoice_url_clipboard_js_render( $post->ID );
		}
	}

	/**
	  * Render invoice payment URL in top of edit invoice screen at backend.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_invoice_url_clipboard_js_render( $post_ID, $column_name = '' ) { 

		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-invoice-url-clipboard-js-admin-display.php' );

	}

	/**
	  * Add Invoice Pay Link head to manage Invoices screen list.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_columns_head_only_invoices( $defaults ) {
		$defaults['invoices_pay_link'] = 'Invoice Pay Link';
		return $defaults;
	}

	/**
	  * Show content of 'invoices_pay_link' column.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_columns_content_only_invoices( $column_name, $post_ID ) {

		$this->p_asapt_invoice_url_clipboard_js_render( $post_ID, $column_name );

	}

	/**
	  * Create an Invoice when a quote is marked as complete and email the link to pay the invoice to the customer. 
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_create_invoice_from_quote( $entry_id, $form, $final_status ) {

		if ( $form['id'] == 6 &&	// Quote Form is ID 6.
			$final_status == 'complete' ) {	
			
			// Get GF Entry.
			$entry = GFAPI::get_entry( $entry_id );

			// Create Invoice.
			$post_ID = wp_insert_post( array(
				'post_type' => 'invoice',
				'post_status' => 'publish',
				'comment_status' => 'closed',
				'ping_status' => 'closed',
				) 
			);

			if ( $post_ID ) {

				$post_ID = wp_update_post( array( 
					'ID' => $post_ID,
					'post_title'   => $post_ID 
					) 
				);

				if ( $entry[2] ) {
					$quoted_file_url = $entry[2];
				} elseif ( $entry[1] ) {
					$quoted_file_url = $entry[1];
				} else {
					$quoted_file_url = '';
				} 

				add_post_meta( $post_ID, 'wpcf-invoice_from_language', $entry[3] );
				add_post_meta( $post_ID, 'wpcf-invoice_to_language', $entry[4] );
				add_post_meta( $post_ID, 'p-meta-quoted-file-url', $quoted_file_url );
				add_post_meta( $post_ID, 'wpcf-invoice_client_first_name', $entry['16.3'] );
				add_post_meta( $post_ID, 'wpcf-invoice_client_last_name', $entry['16.6'] );
				add_post_meta( $post_ID, 'wpcf-invoice_client_email', $entry[10] );
				
				if ( $entry[11] ) {
					add_post_meta( $post_ID, 'wpcf-invoice_client_phone', $entry[11] );
				}

				if ( $entry[31] ) {
					$delivery_method = explode( '|', $entry[31] );
					add_post_meta( $post_ID, 'wpcf-invoice_delivery_method', $delivery_method[0] );
				}

				if ( $entry[28] ) {
					$pages_qty = explode( '|', $entry[28] );
					add_post_meta( $post_ID, 'wpcf-invoice_pages_qty', $pages_qty[0] );
				}

				if ( $entry[30] > 0 ) {
					add_post_meta( $post_ID, 'wpcf-invoice_service_amount', $entry[30] );
				} else {
					add_post_meta( $post_ID, 'wpcf-invoice_service_amount', $entry[29] );
				}

				add_post_meta( $post_ID, 'p-ref-quote-id', $entry['id'] );

			}

			$this->p_asapt_email_invoice_pay_link( $post_ID, $entry );

		}

	}

	/**
	  * Send an invoice link pay by email to customer.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_email_invoice_pay_link( $post_ID, $entry ) {

		// Get client's first name
		$name =  $entry['16.3'];

		// Get client's email
		$to = $entry[10];

		// Get Order Translation page link with the Invoice ID passed as query string
		$order_translation_link = add_query_arg( array(
			'inv' => $post_ID,
			'quote' => $entry['id'],
			), site_url( '/order-translation/' ) );

		// Set email subject
		$subject = 'ASAP Translate - Invoice #' . $post_ID;

		// Set email message
		$message = $this->p_asapt_get_invoice_pay_email_format( $name, $order_translation_link, $entry );

		// Send email
		wp_mail( $to, $subject, $message );
	}

	/**
	  * Set HTML Email Content Type.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_set_html_mail_content_type() {
		return 'text/html';
	}

	/**
	  * Return the Pay Quote/Invoice Email format.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_get_invoice_pay_email_format( $name, $order_translation_link, $entry ) {

		// Set the quote date
		$date = date_create( $entry['date_created'] );

		// Qty of pages to translate.
		$pages_qty = explode( '|', $entry[28] );

		// Set locale for currency format.
		setlocale( LC_MONETARY, 'en_US.UTF-8' );

		$delivery_method = explode( '|', $entry[31] );

		if ( $delivery_method[0] == 'free' ) {
			$delivery_method_option = 'E-mail Only';
			$delivery_method_price = 'Free';
		} else {
			$delivery_method_option = 'E-mailed, Notarized and Mailed by USPS within 2-3 days';
			$delivery_method_price = money_format( '%.2n', $delivery_method[1] );
		}

		ob_start();
		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-invoice-pay-email-format.php' );
		return ob_get_clean();
	}

	/**
	  * Set the WP Email From.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_wp_mail_from( $email_from ) {
		return 'support@asaptranslate.com';
	}

	/**
	  * Set the WP Email From Name.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_wp_mail_from_name( $email_from_name ) {
		return 'ASAP Translate';
	}

	/**
	  * Filter pages quantity field value to not show pricing at all.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_trim_price( $value, $field, $lead, $form ) {
		if ( $form['id'] != 1 || $field->id != 5 )
			return $value;

		$value = substr( $value, 0, -9 );
		
		return $value;
	}


	/**
	  * Return true
	  *
	  * @since    1.0.0
	  */
	public function __p_asapt_return_true() {
		return true;
	}



}