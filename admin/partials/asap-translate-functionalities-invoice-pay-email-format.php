<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/admin/partials
 */
?>
<div bgcolor="#FFFFFF" marginheight="0" marginwidth="0" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;min-height:100%;margin:0;padding:0">
	<table style="width:100%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;background:#548cc5;margin:0;padding:0" bgcolor="#548cc5" width="100%">
		<tbody>
			<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
				<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0"></td>
				<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" align="center">
					<div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:13px 0">
						<table style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
							<tbody>
								<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
									<td style="color:#fff;text-align:center;font-size:18px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" align="center"><?php _e( 'ASAP Translate', 'asap-translate-functionalities' ); ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</td>
				<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0"></td>
			</tr>
		</tbody>
	</table>
	<table style="width:100%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0 0 70px" bgcolor="" cellspacing="0" width="100%">
		<tbody>
			<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
				<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0"></td>
				<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" align="center" bgcolor="#FFFFFF">
					<table style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" cellspacing="0">
						<tbody>
							<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
								<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0;border-color:#dadada;border-style:solid;border-width:0px 1px 1px">
									<div style="max-width:600px;display:block;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0 auto;padding:15px 35px">
										<table style="width:100%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" cellspacing="0" width="100%">
											<tbody>
												<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
													<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:15px 0 0">
														<p style="font-size:15px;color:#222;line-height:22px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-weight:normal;margin:0 0 10px;padding:0">
															Hi <?php echo $name; ?>,<br><br>
															Thank you for choosing ASAP Translate for your translation needs. Our translation team has reviewed your document(s) and provided a guaranteed quote.<br><br>
														</p>
														<div style="margin:0 auto;width:300px;text-align:center">
															<a href="<?php echo $order_translation_link; ?>" style="font-weight:bold;display:block;background:#548cc5;border-radius:5px;margin:0 auto;width:225px;padding:18px;text-align:center;font-size:16px;color:#fff" target="_blank">Pay Now</a>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div style="max-width:600px;display:block;border-top-style:solid;border-top-width:3px;border-top-color:#ececec;font-size:14px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0 auto;padding:35px 35px 15px">
										<table style="width:100%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" cellspacing="0" width="100%">
											<tbody>
												<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
													<td style="vertical-align:top;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" valign="top">
														<div style="color:#929292;font-size:15px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0 0 7px">
															<?php _e( 'Quote Date', 'asap-translate-functionalities' ); ?>
														</div>
														<b style="font-size:14px;display:inline-block;color:#333;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
															<?php echo date_format( $date, 'M jS, Y' ); ?>
														</b>
													</td>
													<td style="vertical-align:top;text-align:right;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" align="right" valign="top">
														<div style="color:#929292;font-size:15px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0 0 7px">
															<?php _e( 'Quote Number', 'asap-translate-functionalities' ); ?>
														</div>
														<b style="font-size:14px;display:inline-block;color:#333;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
															<?php echo $entry['id']; ?>
														</b>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div style="max-width:600px;display:block;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0 auto;padding:45px 35px 60px">
										<table style="width:100%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" bgcolor="" cellspacing="0" width="100%">
											<tbody>
												<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
													<td style="vertical-align:top;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0 0 10px" valign="top">
														<div style="font-weight:600;font-size:13px;color:#a3a3a3;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
															<?php _e( 'Description', 'asap-translate-functionalities' ); ?>
														</div>
													</td>
													<td style="vertical-align:top;text-align:right;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0 0 10px" align="right" valign="top">
														<div style="font-weight:600;font-size:13px;color:#a3a3a3;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
															<?php _e( 'Price', 'asap-translate-functionalities' ); ?>
														</div>
													</td>
												</tr>
												<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
													<td style="border-top-width:3px;border-top-style:solid;border-top-color:#ececec;vertical-align:top;color:#252525;font-size:15px;line-height:23px;width:50%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:18px 0" valign="top">
														<div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
															Translate Documents (<?php echo $pages_qty[0]; ?> pages) <b><?php echo $entry[3]; ?></b> to <b><?php echo $entry[4]; ?></b>
														</div>
													</td>
													<td style="border-top-width:3px;border-top-style:solid;border-top-color:#ececec;vertical-align:top;color:#252525;font-size:15px;line-height:23px;width:16%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:18px 0" valign="top">
														<div style="color:#666;text-align:right;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" align="right">
															<span style="color:#52c375;font-weight:600;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif"><?php echo money_format( '%.2n', $pages_qty[1] ); ?></span>
														</div>
													</td>
												</tr>
												<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
													<td style="border-top-width:3px;border-top-style:solid;border-top-color:#ececec;vertical-align:top;color:#252525;font-size:15px;line-height:23px;width:50%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:18px 0" valign="top">
														<div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
															<?php _e( 'Delivery Method', 'asap-translate-functionalities' ); ?>: <?php echo $delivery_method_option; ?>
														</div>
													</td>
													<td style="border-top-width:3px;border-top-style:solid;border-top-color:#ececec;vertical-align:top;color:#252525;font-size:15px;line-height:23px;width:16%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:18px 0" valign="top">
														<div style="color:#666;text-align:right;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0" align="right">
															<span style="color:#52c375;font-weight:600;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif"><?php echo $delivery_method_price; ?></span>
														</div>
													</td>
												</tr>
												<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
													<td style="width:100%;text-align:right;color:#666;font-size:18px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:25px 0 0" colspan="2" align="right">
														<?php _e( 'Total', 'asap-translate-functionalities' ); ?>
														<span style="display:inline-block;color:#52c375;font-weight:600;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0 0 0 20px">
															<b><?php echo money_format( '%.2n', $entry[29] ); ?></b>
														</span>
													</td>
												</tr>

												<?php if ( $entry[30] ) : ?>
													<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
														<td style="width:100%;text-align:right;color:#666;font-size:18px;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:25px 0 0" colspan="2" align="right">
															<?php _e( 'Special Discount Total', 'asap-translate-functionalities' ); ?>
															<span style="display:inline-block;color:#52c375;font-weight:600;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0 0 0 20px">
																<b><?php echo money_format( '%.2n', $entry[30] ); ?></b>
															</span>
														</td>
													</tr>
												<?php endif; ?>

											</tbody>
										</table>
									</div>
									<div style="max-width:600px;display:block;border-top-color:#e9ebed;border-top-style:solid;border-top-width:1px;font-size:14px;text-align:center;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;background:#f8f9fa;margin:0 auto;padding:15px 35px" align="center">
										<table style="width:100%;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:25px 0" bgcolor="" cellspacing="0" width="100%">
											<tbody>
												<tr style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
													<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
														<div style="color:#6a6c70;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0 0 8px">
															If you have any questions about your quote, please contact us via email
														</div>
														<span style="color:#61a1e1;text-decoration:underline;font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0">
															<a href="mailto:support@asaptranslate.com" target="_blank">support@asaptranslate.com</a>
														</span>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;margin:0;padding:0"></td>
			</tr>
		</tbody>
	</table>
</div>
