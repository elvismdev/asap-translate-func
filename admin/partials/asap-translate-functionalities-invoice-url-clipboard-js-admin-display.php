<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/admin/partials
 */
?>

<div class="<?php echo ( $column_name == 'invoices_pay_link' ? 'manage-invoices-screen' : ''); ?> invoice-payment-link input-group">
	<input id="invoice-payment-url-<?php echo $post_ID; ?>" type="text" value="<?php echo site_url(); ?>/order-translation/?inv=<?php echo $post_ID; ?>" readonly>
	<span class="input-group-button">
		<span class="copy-link" data-clipboard-target="#invoice-payment-url-<?php echo $post_ID; ?>">
			<img class="clippy" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/clippy.svg" width="13" alt="Copy to clipboard">
		</span>
	</span>
</div>
