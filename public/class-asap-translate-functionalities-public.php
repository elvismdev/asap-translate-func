<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/public
 * @author     Parenthesis Tech <team@parenthesis.io>
 */
class Asap_Translate_Functionalities_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Asap_Translate_Functionalities_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Asap_Translate_Functionalities_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/asap-translate-functionalities-public.css', array(), $this->version, 'all' );



		// if ( is_page( 14 ) ) {

		// 	wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . '../assets/vendor/sweetalert/dist/sweetalert.css', array(), $this->version, 'all' );
		// 	wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . '../assets/vendor/alertifyjs/dist/css/alertify.css', array(), $this->version, 'all' );
		
		// }

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Asap_Translate_Functionalities_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Asap_Translate_Functionalities_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/asap-translate-functionalities-public.js', array( 'jquery' ), $this->version, false );

		// if ( is_page( 14 ) ) {
		// 	wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . '../assets/vendor/sweetalert/dist/sweetalert.min.js', array( 'jquery' ), $this->version, false );
		// 	wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . '../assets/vendor/alertifyjs/dist/js/alertify.js', array( 'jquery' ), $this->version, false );
		// }


	}

	/**
	 * Add invoice query var to fetch the invoice data and set the fields values for the Order Translate form.
	 *
	 * @since    1.0.0
	 */
	public function p_asapt_add_invoice_query_var( $vars ) {
		$vars[] = 'inv';
		$vars[] = 'quote';
		$vars[] = 'fn';
		$vars[] = 'ln';
		$vars[] = 'em';
		$vars[] = 'ph';
		$vars[] = 'ship';
		return $vars;
	}

	/**
	  * Set the total for the invoice to charge.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_invoice_amount() {
		global $post;
		$invoice_ammount =  get_post_meta( $post->ID, 'wpcf-invoice_service_amount', true );
		return esc_attr( number_format( $invoice_ammount, 2 ) );
	}

	/**
	  * Set the client Email taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_email() {
		$client_email = get_query_var( 'em' );
		if ( $client_email ) 
			return esc_attr( $client_email );

		$invoice_id = get_query_var( 'inv' );
		$client_email = get_post_meta( $invoice_id, 'wpcf-invoice_client_email', true );
		return esc_attr( $client_email );
	}

	/**
	  * Set the From Language value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_from_language() {
		$invoice_id = get_query_var( 'inv' );
		$from_language = get_post_meta( $invoice_id, 'wpcf-invoice_from_language', true );
		return esc_attr( $from_language );
	}

	/**
	  * Set the To Language value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_to_language() {
		$invoice_id = get_query_var( 'inv' );
		$to_language = get_post_meta( $invoice_id, 'wpcf-invoice_to_language', true );
		return esc_attr( $to_language );
	}

	/**
	  * Set the First Name value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_first_name() {
		$first_name = get_query_var( 'fn' );
		if ( $first_name ) 
			return esc_attr( $first_name );
		
		$invoice_id = get_query_var( 'inv' );
		$first_name = get_post_meta( $invoice_id, 'wpcf-invoice_client_first_name', true );
		return esc_attr( $first_name );
	}

	/**
	  * Set the Last Name value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_last_name() {
		$last_name = get_query_var( 'ln' );
		if ( $last_name ) 
			return esc_attr( $last_name );

		$invoice_id = get_query_var( 'inv' );
		$last_name = get_post_meta( $invoice_id, 'wpcf-invoice_client_last_name', true );
		return esc_attr( $last_name );
	}

	/**
	  * Set the Phone Number value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_phone() {
		$phone = get_query_var( 'ph' );
		if ( $phone ) 
			return esc_attr( $phone );

		$invoice_id = get_query_var( 'inv' );
		$phone = get_post_meta( $invoice_id, 'wpcf-invoice_client_phone', true );
		return esc_attr( $phone );
	}

	/**
	  * Set the Street Address value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_street_address() {
		$invoice_id = get_query_var( 'inv' );
		$street_address = get_post_meta( $invoice_id, 'wpcf-invoice_client_street_address', true );
		return esc_attr( $street_address );
	}

	/**
	  * Set the City value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_city() {
		$invoice_id = get_query_var( 'inv' );
		$city = get_post_meta( $invoice_id, 'wpcf-invoice_client_city', true );
		return esc_attr( $city );
	}

	/**
	  * Set the State value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_state() {
		$invoice_id = get_query_var( 'inv' );
		$state = get_post_meta( $invoice_id, 'wpcf-invoice_client_state_province_region', true );
		return esc_attr( $state );
	}

	/**
	  * Set the Zip value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_zip() {
		$invoice_id = get_query_var( 'inv' );
		$zip = get_post_meta( $invoice_id, 'wpcf-invoice_client_zip_postal_code', true );
		return esc_attr( $zip );
	}

	/**
	  * Set the Country value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_country() {
		$invoice_id = get_query_var( 'inv' );
		$country = get_post_meta( $invoice_id, 'wpcf-invoice_client_country', true );
		return esc_attr( $country );
	}

	/**
	  * Set the Delivery value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_delivery() {
		$invoice_id = get_query_var( 'inv' );
		$delivery = get_post_meta( $invoice_id, 'wpcf-invoice_delivery_method', true );
		return esc_attr( $delivery );
	}

	/**
	  * Set the Additional Info value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_additional_info() {
		$invoice_id = get_query_var( 'inv' );
		$add_info = get_post_meta( $invoice_id, 'wpcf-invoice_additional_info', true );
		return esc_attr( $add_info );
	}

	/**
	  * Set the Invoice Pages to Translate value taken it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_invoice_pages_to_translate() {
		$invoice_id = get_query_var( 'inv' );
		$pages_to_translate = get_post_meta( $invoice_id, 'wpcf-invoice_pages_qty', true );
		return esc_attr( $pages_to_translate );
	}





	/**
	  * Set the Ammount to charge taking it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_invoice_amount() {
		$invoice_id = get_query_var( 'inv' );
		$invoice_ammount =  get_post_meta( $invoice_id, 'wpcf-invoice_service_amount', true );
		return esc_attr( number_format( $invoice_ammount, 2 ) );
	}

	/**
	  * Set the Delivery Method taking it from the Invoice data.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_gf_filter_order_translation_invoice_delivery_method() {
		$delivery_method = get_query_var( 'ship' );

		if ( $delivery_method == 'Please send me a physical copy.' ) {
			$delivery_method = 'shipusps';
			return esc_attr( $delivery_method );
		}
		

		$quote_id = get_query_var( 'quote' );
		if ( $quote_id ) {
			$invoice_id = get_query_var( 'inv' );
			$delivery_method =  get_post_meta( $invoice_id, 'wpcf-invoice_delivery_method', true );
			return esc_attr( $delivery_method );
		} else {
			return;
		}
	}


	/**
	  * Hide and do not process the select field Pages to translate if we come to charge an Invoice.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_set_conditional_logic_pages_to_translate( $form ) {
		foreach ( $form['fields'] as &$field ) {
			if ( $field->id == 5 ) {
				$field->conditionalLogic =
				array(
					'actionType' => 'hide',
					'logicType' => 'all',
					'rules' =>
					array( array( 'fieldId' => 20, 'operator' => '>', 'value' => '0' ) )
					);
			}
		}
		return $form;
	}

	/**
	  * Hide and do not process the Invoice Total field if we DO NOT come to charge an Invoice.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_set_conditional_logic_invoice_total( $form ) {
		foreach ( $form['fields'] as &$field ) {
			if ( $field->id == 21 ) {
				$field->conditionalLogic =
				array(
					'actionType' => 'hide',
					'logicType' => 'all',
					'rules' =>
					array( array( 'fieldId' => 20, 'operator' => 'is', 'value' => '0' ) )
					);
			}
		}
		return $form;
	}

	/**
	  * Hide and do not process the Invoice Pages to Translate field if we DO NOT come to charge an Invoice.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_set_conditional_pages_to_translate( $form ) {
		foreach ( $form['fields'] as &$field ) {
			if ( $field->id == 22 ) {
				$field->conditionalLogic =
				array(
					'actionType' => 'hide',
					'logicType' => 'all',
					'rules' =>
					array( array( 'fieldId' => 20, 'operator' => 'is', 'value' => '0' ) )
					);
			}
		}
		return $form;
	}

	/**
	  * Hide and do not process the Delivery Method field if we come to change an Invoice/Quote.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_set_conditional_delivery_method( $form ) {
		$quote_id = get_query_var( 'quote' );
		if ( $quote_id ) {		
			foreach ( $form['fields'] as &$field ) {
				if ( $field->id == 15 ) {
					$field->conditionalLogic =
					array(
						'actionType' => 'hide',
						'logicType' => 'all',
						'rules' =>
						array( array( 'fieldId' => 20, 'operator' => '>', 'value' => '0' ) )
						);
				}
			}
		}
		return $form;
	}

	/**
	  * Set Readonly the Invoice Pages to Translate.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_add_readonly_script( $form ) {
		include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-read-only-field-public-display.php' );
		return $form;
	}

	/**
	  * Add class gf_invisible to Total field in Quote Form Fronted only.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_add_gf_invisible( $classes, $field, $form ) {
		if ( $field->id == 29 && ! is_admin() ) {
			$classes .= ' gf_invisible';
		}
		return $classes;
	}


	/**
	  * Add class gf_invisible to Total field in Quote Form Fronted only.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_clear_dollar_sign( $value, $merge_tag, $options, $field, $raw_value ) {

		if ( $field->id == 7 ) {
			return str_replace( '$', '' ,$value );
		}

		return $value;

	}


	/**
	  * Custom page title for /certified-translation page.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_filter_page_title_tag( $title ) {

		if ( is_page( 161 ) ) {
			$title = 'Certified Translations | ASAP Translate';
		}

		return $title;

	}


	/**
	  * Prevent to close the order translation tab with JS.
	  *
	  * @since    1.0.0
	  */
	public function p_asapt_alert_on_tab_close() {

		if ( is_page( array( 14, 2, 309 ) ) ) {
			include( plugin_dir_path( __FILE__ ) . 'partials/' . $this->plugin_name . '-alert-on-close-public-display.php' );
		}

	}




}
