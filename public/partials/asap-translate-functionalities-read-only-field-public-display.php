<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/public/partials
 */
?>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("li.gf_readonly input").attr("readonly","readonly");
	});
</script>
