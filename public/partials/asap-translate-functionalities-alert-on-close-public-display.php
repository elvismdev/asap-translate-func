<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://parenthesis.io/
 * @since      1.0.0
 *
 * @package    Asap_Translate_Functionalities
 * @subpackage Asap_Translate_Functionalities/public/partials
 */
?>

<script type="text/javascript">


	jQuery(document).ready(function () {
		jQuery(window).on('beforeunload', function(){
			return "Your order is not complete!\n\nIf you leave, your information will be lost and you will have to start over.\n\nIf you need help, our live chat support is here to assist you.";
		});
		jQuery(document).on("submit", "form", function(event){
			jQuery(window).off('beforeunload');
		});
	});


</script>
